unit main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  petalclass;

type

  { TForm1 }

  TForm1 = class(TForm)
    Img: TImage;
    Timer1: TTimer;
    procedure Timer1Timer(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form1: TForm1;
  Marg: boolean;
  Petals: array [0..49] of TPetal;
  CurPetal: smallint;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Timer1Timer(Sender: TObject);
begin
     if not Marg then
       begin
            Petals[CurPetal]:=TPetal.Create(img.Width,img.Height);
            Petals[CurPetal].Draw(img.Canvas);
            CurPetal+=1;
            if CurPetal=50 then Marg:=TRUE;
       end else begin
            Petals[50-CurPetal].Draw(img.Canvas,TRUE);
            Petals[50-CurPetal].Free;
            CurPetal-=1;
            if CurPetal=0 then Marg:=FALSE;
       end;
     img.Canvas.TextOut(10,10,IntToStr(CurPetal)+'  ');
end;

end.

